" Matt Gorle's personal .vimrc.
" -----------------------------
" Version 1.0: 2015-08-03
" Version 1.1: 2015-08-07:
"   - Added Vundle
"   - Added vim-airline
"   - Added syntastic
" Version 1.2: 2015-08-07:
"   - Added tagbar
" Version 1.3: 2015-08-07:
"   - Added easytags
" Version 1.4: 2015-08-26:
"   - Added fonts for gVim
"   - Removed Menu in gVim
"   - Added php-documentor

" No compatibility with VI
set nocompatible

" Required by vundle
filetype off

" Assume 256 colour support in terminal (for TMUX)
set t_Co=256

" Fonts
if has("gui_running")
    if has("gui_win32")
        set guifont=Sauce_Code_Powerline:h12:cANSI
    endif
endif

" Disable menus and toolbar
set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=L

" Enable Syntax Highlighting
syntax on

" Enable Line Numbering
set number

" Try to detect filetypes and load plugins automatically
filetype on
filetype plugin on
filetype indent on

" Reload files that are changed outside vim
set autoread

" Set default file encoding and line-end format
set encoding=utf-8
set fileencoding=utf-8

set fileformat=unix


" Indentation
set expandtab		" use spaces instead of tabs
set autoindent		" autoindent based on line above
set smartindent		" smarter indent for C-style languages
set shiftwidth=4	" when reading files, tabs are 4 spaces
set softtabstop=4	" in insert mode, tabs are 4 spaces

" no lines longer than 80 chars, ruler at 80th column
set textwidth=80
set colorcolumn=80
highlight ColorColumn ctermbg=darkgray

"colorscheme wombat256
"colorscheme sourcerer
"colorscheme znake
colorscheme mustang

" remap leader to backquote (`)
let mapleader="`"
let g:mapleader="`"

" folding

set foldmethod=indent
set foldnestmax=10
set foldlevel=1
highlight Folded ctermbg=none

" vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
set laststatus=2

" Syntastic
let g:syntastic_php_phpcs_args='--standard=psr2'

" Tagbar
nmap <F8> : TagbarToggle<CR>

" easytags
set tags=./tags
let g:easytags_dynamic_files = 1

" php-documentor
au BufRead,BufNewFile *.php inoremap <buffer> <leader>p :call PhpDoc()<CR>
au BufRead,BufNewFile *.php nnoremap <buffer> <leader>p :call PhpDoc()<CR>
au BufRead,BufNewFile *.php vnoremap <buffer> <leader>p :call PhpDocRange()<CR>
let g:pdv_cfg_Author = 'Matt Gorle <matt@gorle.co.uk>'
let g:pdv_cfg_ClassTags = ["author","version"]
" Vundle

set rtp+=$HOME/.vim/bundle/Vundle.vim/

call vundle#begin()
Plugin 'gmarik/Vundle.vim'
Plugin 'scrooloose/syntastic'
Plugin 'bling/vim-airline'
Plugin 'majutsushi/tagbar'
Plugin 'xolox/vim-misc'
Plugin 'xolox/vim-easytags'
Plugin 'vitalk/vim-simple-todo'
Plugin 'edkolev/tmuxline.vim'
Plugin 'evidens/vim-twig'
Plugin 'sumpygump/php-documentor-vim'
call vundle#end()
